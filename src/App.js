import React from "react";
import './App.css';
import Register from "./components/containers/Register";
import Login from "./components/containers/Login";
import Dashboard from "./components/containers/Dashboard";
import NotFound from "./components/containers/NotFound";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { getStorage } from "./utils/localStorage";
import { useDispatch } from "react-redux";
import { setSessionAction } from "./store/actions/sessionAction";

function App() {

  const localSession = getStorage("React_session");
  const dispatch = useDispatch();
  dispatch(setSessionAction(localSession));

  return (

    <Router>
      <div className="App">
        My React App
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/dashboard" component={Dashboard} />
          <Route path="*" component={NotFound} />
        </Switch>

      </div>
    </Router>

  );
}

export default App;

