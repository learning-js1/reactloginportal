import { SET_SURVEYS, ADD_SURVEY, DELETE_SURVEY, UPDATE_SURVEY } from "./actionTypes";

export const setSurveysAction = (surveys = []) => ({
    type: SET_SURVEYS,
    surveys
});

export const addSurveyAction = (survey) => ({
    type: ADD_SURVEY,
    survey
});

export const deleteSurveyAction = (survey_Id) => ({
    type: DELETE_SURVEY,
    survey_Id
});

export const updateSurveyAction = (survey) => ({
    type: UPDATE_SURVEY,
    survey
});