import { SET_SESSION, CLEAR_SESSION } from "./actionTypes";

export const setSessionAction = session => ({
    type: SET_SESSION,
    session
});

export const clearSessionAction = () => ({
    type: CLEAR_SESSION
})