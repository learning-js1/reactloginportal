export const SET_SURVEYS = "SET_SURVEYS";
export const ADD_SURVEY = "ADD_SURVEY";
export const DELETE_SURVEY = "DELETE_SURVEY";
export const UPDATE_SURVEY = "UPDATE_SURVEY";

export const SET_SESSION = "SET_SESSION";
export const CLEAR_SESSION = "CLEAR_SESSION";