import { combineReducers } from "redux";
import surveyReducer from "./surveyReducer";
import sessionReducer from "./sessionReducer";

export default combineReducers({
    surveys: surveyReducer,
    session: sessionReducer
});
