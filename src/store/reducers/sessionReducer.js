import { setStorage } from "../../utils/localStorage";

const { SET_SESSION, CLEAR_SESSION } = require("../actions/actionTypes")

const initialState = {
    token: "",
    user: {}
}

const sessionReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case SET_SESSION:
            return action.session;
        case CLEAR_SESSION: {
            setStorage("React_session", initialState);
            return initialState;
        }

        default:
            return state;
    }
}

export default sessionReducer;