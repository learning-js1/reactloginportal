import { ADD_SURVEY, SET_SURVEYS, DELETE_SURVEY, UPDATE_SURVEY } from "../actions/actionTypes";

const surveyReducer = (state = [], action = {}) => {
    switch (action.type) {
        case SET_SURVEYS:
            return [...action.surveys];
        case ADD_SURVEY:
            return [
                ...state,
                action.survey
            ]
        case DELETE_SURVEY: {

            const filteredSurveys = state.filter(survey => survey.survey_id !== action.survey_Id);
            return [
                ...filteredSurveys
            ];
        }
        case UPDATE_SURVEY: {
            return [
                ...state
            ]
        }
        default:
            return state;
    }

}

export default surveyReducer;