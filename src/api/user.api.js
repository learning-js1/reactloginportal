import { handleResponse } from "./api";

const API_REGISTER_URL = "https://survey-poodle.herokuapp.com/v1/api/users/register";
const API_LOGIN_URL = "https://survey-poodle.herokuapp.com/v1/api/users/login";

const createFetchOptions = (method, body) => ({
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(body)
});

export const registerUser = (username, password) => {
    return fetch(
        API_REGISTER_URL,
        createFetchOptions("POST", {
            user: {
                username,
                password
            }
        })
    ).then(response => response.json())
        .then(handleResponse)
}

export const loginUser = (username, password) => {
    return fetch(
        API_LOGIN_URL,
        createFetchOptions("POST", {
            user: {
                username,
                password
            }
        })
    ).then(response => response.json())
        .then(handleResponse)
}