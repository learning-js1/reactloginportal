import axios from "axios";
import { BASE_URL, handleResponse } from "./api";

export const getSurveys = () => {
    return axios.get(`${BASE_URL}/surveys`)
        .then(handleResponse)
        .then(response => response.data);
}

