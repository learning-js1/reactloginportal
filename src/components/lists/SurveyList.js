import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getSurveys } from "../../api/surveysApi";
import { setSurveysAction } from "../../store/actions/surveyAction";

const SurveyList = () => {

    const dispatch = useDispatch();
    const surveys = useSelector(state => state.surveys);

    useEffect(() => {
        getSurveys().then(({ data, status, error }) => {

            dispatch(setSurveysAction(data))

        }).catch(error => {
            console.error(error);
        });
    }, [dispatch]);

    const surveyList = surveys.map(survey => (
        <li key={survey.survey_id}>
            <h4>{survey.title}</h4>
            <p>{survey.description}</p>
            <small>Created by: {survey.user.username}</small>
        </li>
    ))

    return (
        <div>
            <ul>
                {surveyList}
            </ul>
        </div>
    )

}

export default SurveyList;