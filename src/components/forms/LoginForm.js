import React, { useState } from "react";
import { loginUser } from "../../api/user.api";

const LoginForm = (props) => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const [loginError, setLoginError] = useState("");

    const onLoginClicked = async () => {

        setIsLoading(true);
        setLoginError("");
        let loginResult;

        try {
            loginResult = await loginUser(username, password);
        } catch (error) {
            setLoginError(error.message || error);
        } finally {
            setIsLoading(false);
            props.complete(loginResult.data);
        }

    }

    const onUsernameChange = event => setUsername(event.target.value.trim());
    const onPasswordChange = event => setPassword(event.target.value.trim());

    return (
        <form>
            <div>
                <label>Username: </label>
                <input type="text" placeholder="Enter your username" onChange={onUsernameChange} />
            </div>

            <div>
                <label>Password: </label>
                <input type="password" placeholder="Enter your password" onChange={onPasswordChange} />
            </div>

            <div>
                <button type="button" onClick={onLoginClicked}>Login</button>
            </div>

            {isLoading && <p>Logging in...</p>}
            {loginError && <p>{loginError}</p>}
        </form>
    )
};

export default LoginForm;