import React, { useState } from "react";
import { registerUser } from "../../api/user.api";

const RegisterForm = props => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const [registerError, setRegisterError] = useState("");

    const onRegisterClicked = async event => {

        setIsLoading(true);
        let result;

        try {
            result = await registerUser(username, password);
        } catch (error) {
            setRegisterError(error.message || error);
        } finally {
            setIsLoading(false);
            props.complete(result || {});
        }
    };

    const onUsernameChanged = event => setUsername(event.target.value.trim());
    const onPasswordChanged = event => setPassword(event.target.value.trim());



    return (
        <form>
            <div>
                <label>Username: </label>
                <input type="text" placeholder="Enter a username" onChange={onUsernameChanged} />
            </div>

            <div>
                <label>Password: </label>
                <input type="password" placeholder="Enter a password" onChange={onPasswordChanged} />
            </div>

            <div>
                <button type="button" onClick={onRegisterClicked}>Register</button>
            </div>

            {isLoading && <div>Registering user...</div>}

            {registerError && <div>{registerError}</div>}


        </form >
    )
};

export default RegisterForm;