import React from "react";

const ProfileCard = ({ user, logout }) => (
    <div className="card">
        <div className="card-body">
            <h5 className="card-title">{user.username}</h5>
            <h6 className="card-subtitle mb-2">{user.updated_at}</h6>
            <button type="button" className="btn btn-light" onClick={() => logout()}>Logout</button>
        </div>
    </div>
);

export default ProfileCard;