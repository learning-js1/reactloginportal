import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { getStorage } from "../../utils/localStorage";
import ProfileCard from "../cards/profileCard";
import SurveyList from "../lists/SurveyList";
import { clearSessionAction } from "../../store/actions/sessionAction";

const Dashboard = () => {

    const { token } = getStorage("React_session");
    const { user } = useSelector(state => state.session);
    const dispatch = useDispatch();

    const handleLogout = () => {
        const doLogout = global.confirm("Are you sure you want to logout?");
        if (doLogout) {
            dispatch(clearSessionAction());
        }
    }


    return (<div className="container">
        <h1>Welcome to the Dashboard.</h1>
        {!token ? <Redirect to="/login" /> :
            <div className="row">
                <aside className="col-4">
                    <ProfileCard user={user} logout={handleLogout} />
                </aside>
                <main className="col-8">
                    <SurveyList />
                </main>
            </div>
        }
    </div>)
};

export default Dashboard;