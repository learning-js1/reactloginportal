import React from "react";
import LoginForm from "../forms/LoginForm";
import { Link, Redirect, useHistory } from "react-router-dom";
import { getStorage, setStorage } from "../../utils/localStorage";
import { useDispatch } from "react-redux";
import { setSessionAction } from "../../store/actions/sessionAction";

const Login = () => {

    const { token } = getStorage("React_session");
    const history = useHistory();
    const dispatch = useDispatch();

    const onLoginComplete = ({ user, token }) => {
        const session = { user, token };
        setStorage("React_session", session);
        dispatch(setSessionAction(session));
        history.replace("/dashboard");
    }

    return (
        <div>
            {token && <Redirect to="/dashboard" />}
            <h1>Login to Survey Puppy.</h1>

            <LoginForm complete={onLoginComplete} />

            <Link to="/register">Not registered? Register here</Link>
        </div>
    )
};

export default Login; 